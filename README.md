# RHME3Challenges

All of the challenges for the Rhme 3 CTF will be posted here.
Before you start the challenges, make sure you personalize the board first with the setup.hex file

This only works on the Riscure provided boards.

![](rhme3overview.png)
